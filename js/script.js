"use strict";

document.addEventListener('DOMContentLoaded', () => {

    const movieDB = {
        movies: [
            "Один дома",
            "Марсианин",
            "Никогда не сдавайся!",
            "Убить Билла"
        ]
    };






    const movieList = document.querySelector('.film-list');
    const addForm = document.querySelector('form.add'),
        addInput = addForm.querySelector('.add-input'),
        checkbox = addForm.querySelector('[type="checkbox"]');


    addForm.addEventListener('submit', (event) => {
        event.preventDefault();

        let newFilm = addInput.value;
        const favoriteMovie = checkbox.checked;

        if (newFilm) {


            if (newFilm.length > 21) {

                newFilm = `${newFilm.substring(0, 22)}...`;
            }

            if (favoriteMovie == true) {
                console.log('Добавляем любимый фильм');
            }
            movieDB.movies.push(newFilm);
            sortArr(movieDB.movies);

            createMovieList(movieDB.movies, movieList);



        }



        event.target.reset();

    });


    const sortArr = (arr) => {
        arr.sort();
    };




    function createMovieList(childFilms, parent) {


        parent.innerHTML = ""; // удаляем список фильмов в html

        sortArr(childFilms); // сортировка списка movieDB.movies

        childFilms.forEach((item, i) => {
            parent.innerHTML +=
                `<li class="film-item">${i + 1}. ${item}
                <div class="delete"></div>
                </li>`;
        }); // записываем фильмы из массива 

        document.querySelectorAll('.delete').forEach((bttn, i) => {

            bttn.addEventListener('click', () => {
                bttn.parentElement.remove();
                movieDB.movies.splice(i, 1);
                createMovieList(movieDB.movies, movieList);
            });

        });


    }
    createMovieList(movieDB.movies, movieList);




});

